import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DemoMaterialModule } from '../demo-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FileUploadRoutes } from './fileupload.routing';
import { ChartistModule } from 'ng-chartist';
import { MatDialogModule } from '@angular/material/dialog';
import { ReactiveFormsModule,FormsModule} from '@angular/forms'

@NgModule({
	imports: [
		CommonModule,
		DemoMaterialModule,
		FlexLayoutModule,
		ChartistModule,
		MatDialogModule,
		RouterModule.forChild(FileUploadRoutes),
		ReactiveFormsModule,
		FormsModule
	],
	declarations: []
})
export class FileuploadModule {}
