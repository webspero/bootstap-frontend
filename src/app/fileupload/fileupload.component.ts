import { Component, OnInit, Inject } from '@angular/core';
import { Papa } from 'ngx-papaparse';
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ExportToCsv } from 'export-to-csv';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms'
import { ApiServiceService } from '../Service/api-service.service'
import { UpdatedataService } from '../Service/updatedata.service'
import Swal, { SweetAlertIcon } from 'sweetalert2'
import { ignoreElements } from 'rxjs/operators';

@Component({
	selector: 'app-fileupload',
	templateUrl: './fileupload.component.html',
	styleUrls: ['./fileupload.component.css']
})
export class FileuploadComponent implements OnInit {
	csvValues = [];
	headers = [];
	csvData;
	availableHeader: any = [];
	sortedHeader = [];
	headersList;
	listedCsv = [];
	unlistedCsv = [];
	sectionAdd = false;
	defaultHeader: any = [];
	public cNum: string;
	public uId: string;
	public columndata: any = []
	public insertCsvFile: string = ''
	public loading: boolean = false
	public currImportType: string = 'archive';
	public showInsertBtn: boolean = false;

	constructor(
		private papa: Papa,
		private activatedRoute: ActivatedRoute,
		public dialog: MatDialog,
		public apiService: ApiServiceService,
		public updateData: UpdatedataService
	) {
		this.activatedRoute.queryParams.subscribe(params => {
			this.cNum = params['cNum'];
			console.log(this.cNum);
			this.uId = params['uid'];
			console.log(this.uId);
		});
	}

	ngOnInit(): void { }

	ngDoCheck() {
		this.updateData.columnData.subscribe((data: any) => {
			if (data) {
				this.columndata = data
			}
		})
		this.updateData.genderData.subscribe((res: any) => {
			if (res && res.changeGenderArray && res.changeGenderArray.length > 0) {
				var index = this.headersList.indexOf(res.header)
				this.csvData.map(ele => {
					for (var i = 0; i < res.changeGenderArray.length; i++) {
						if (ele[index] === res.changeGenderArray[i].name) {
							console.log(ele[index], "===", res.changeGenderArray[i].name)
							ele.splice(index, 1, res.changeGenderArray[i].value)
						}
					}
				})
			}

			
			if (res && res.changeEmailArray && res.changeEmailArray.length > 0) {
				var index = this.headersList.indexOf(res.header)
				this.csvData.map(ele => {
					for (var i = 0; i < res.changeEmailArray.length; i++) {
						if (ele[index] === res.changeEmailArray[i].name) {
							console.log(ele[index], "===", res.changeEmailArray[i].name)
							ele.splice(index, 1, res.changeEmailArray[i].value)
						}
					}
				})
			}

			
			if (res && res.changeSmsArray && res.changeSmsArray.length > 0) {
				var index = this.headersList.indexOf(res.header)
				this.csvData.map(ele => {
					for (var i = 0; i < res.changeSmsArray.length; i++) {
						if (ele[index] === res.changeSmsArray[i].name) {
							console.log(ele[index], "===", res.changeSmsArray[i].name)
							ele.splice(index, 1, res.changeSmsArray[i].value)
						}
					}
				})
			}
		})
	}

	greater_than_zero(totn_element) {
		return totn_element > 0;
	}

	checkMapingValue(value) {
		var index = this.columndata.findIndex(ele => ele.name.toLowerCase() === value.toLowerCase())
		return index === -1 ? false : true
	}

	getUnique(valmatch) {
		if (this.sortedHeader.indexOf(valmatch)) {
			return 'not-classmatched';
		} else {
			return 'classmatched';
		}
	}

	ifHeaderSet(valmatch) {
		var index = this.columndata.findIndex(ele => ele.name.toLowerCase() === valmatch.toLowerCase())
		if (index === -1) {
			return 'header-not-set';
		} else {
			return 'header-set';
		}
	}

	openDialog(headers, indx): void {
		var selectedColumn = { name: "", value: "" };
		console.log();
		this.columndata.findIndex(function (element) {
			if (element.name.toLowerCase() === headers.toLowerCase()) {
				selectedColumn = element;
			}
		});
		let data = {
			'csvData': this.csvData,
			'headersList': this.headersList,
			'currentHeader': headers,
			'availableHeader': this.availableHeader,
			'openModalHeader': headers.toLowerCase(),
			'indx': indx,
			'selectedColumn': selectedColumn,
			'columnData': this.columndata,
			'currImportType': this.currImportType
		}
		const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
			width: '50%',
			data: data
		}
		);

		dialogRef.afterClosed().subscribe(result => {
		});
	}

	selectedValue(currentHeader) {
		console.log('FirstName');
		return 'FirstName'
	}


	compareCsvField(csvArray, databaseArray) {
		const finalarray = [];
		csvArray.forEach((e1) => databaseArray.forEach((e2) => {
			if (e1 === e2) {
				finalarray.push(e1);
			}
		}));
		this.sortedHeader = finalarray;
	}

	filterData(type, arrayVal, indexval) {
		arrayVal = arrayVal.filter(function (item, index) {
			if (index > 0) {
				return item;
			}
		});
		var listedData = arrayVal;
		var oldArray = arrayVal
		var unlistedCsv = [];
		var listedCsv = [];
		if (type != "") {
			this.headers.filter(function (item, index) {
				if (index > 0) {
					if (type === 'email') {
						var emailField = item[4];
						var EMAIL_REGEXP = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/i;

						if (emailField == '') {
							unlistedCsv.push(item);
						} else if (emailField != "" && (emailField.length <= 5 || !EMAIL_REGEXP.test(emailField))) {
							unlistedCsv.push(item);
						} else {
							listedCsv.push(item);
						}
					} else {
						if (item[indexval] != '') {
							listedCsv.push(item);
						}
					}
					listedData = listedCsv;
				}
			});
		} else {
			listedData = oldArray;
		}
		this.csvData = listedData;
	}

	sortData(type, arrayVal, indexval) {
		arrayVal = arrayVal.filter(function (item, index) {
			if (index > 0) {
				return item;
			}
		});
		var listedData = arrayVal;
		var oldArray = arrayVal
		var unlistedCsv = [];
		var listedCsv = [];
		if (type != "") {
			this.headers.filter(function (item, index) {
				if (index > 0) {
					if (type === 'email') {
						var emailField = item[4];
						var EMAIL_REGEXP = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/i;

						if (emailField == '') {
							unlistedCsv.push(item);
						} else if (emailField != "" && (emailField.length <= 5 || !EMAIL_REGEXP.test(emailField))) {
							unlistedCsv.push(item);
						} else {
							listedCsv.push(item);
						}
					} else {
						if (item[indexval] != '') {
							listedCsv.push(item);
						}
					}
					listedData = listedCsv;
				}
			});
		} else {
			listedData = oldArray;
		}
		this.csvData = listedData;
	}

	csvDownload() {
		const options = {
			fieldSeparator: ',',
			quoteStrings: '"',
			decimalSeparator: '.',
			showLabels: true,
			showTitle: false,
			title: 'My Awesome CSV',
			useTextFile: false,
			useBom: true,
			useKeysAsHeaders: false,
			headers: this.headersList
		};
		const csvExporter = new ExportToCsv(options);
		csvExporter.generateCsv(this.csvData);
	}

	selectFile(id) {
		document.getElementById(id).click()
		this.insertCsvFile = id
		console.log(id);

		if (id === "lead") {
			this.currImportType = 'lead';
			this.availableHeader = [
				{ name: "First Name", value: "FirstName" },
				{ name: "Last Name", value: "LastName" },
				{ name: "Gender", value: "Gender" },
				{ name: "Date of Birth", value: "Dob" },
				{ name: "Contact Phone", value: "ContactPhone" },
				{ name: "Email", value: "Email" },
				{ name: "Get Email", value: "GetEmail" },
				{ name: "Get SMS", value: "GetSMS" },
				{ name: "Pipeline", value: "Pipeline" },
				{ name: "Source", value: "Source" },
				{ name: "Status", value: "Status" }
			]
		} else if (id === "active") {
			this.currImportType = 'active';
			this.availableHeader = [
				{ name: "First Name", value: "FirstName" },
				{ name: "Last Name", value: "LastName" },
				{ name: "Gender", value: "Gender" },
				{ name: "Date of Birth", value: "Dob" },
				{ name: "Contact Phone", value: "ContactPhone" },
				{ name: "Email", value: "Email" },
				{ name: "Get Email", value: "GetEmail" },
				{ name: "Get SMS", value: "GetSMS" },
				{ name: "Membership", value: "MemberShip" },
				{ name: "Start Date", value: "StartDate" },
				{ name: "Valid Date", value: "VaildDate" },
				{ name: "True Balance Value", value: "TrueBalanceValue" }
			]
		} else {
			this.currImportType = 'archive';
			this.availableHeader = [
				{ name: "First Name", value: "FirstName" },
				{ name: "Last Name", value: "LastName" },
				{ name: "Gender", value: "Gender" },
				{ name: "Date of Birth", value: "Dob" },
				{ name: "Contact Phone", value: "ContactPhone" },
				{ name: "Email", value: "Email" },
				{ name: "Get Email", value: "GetEmail" },
				{ name: "Get SMS", value: "GetSMS" }
			]
		}
	}

	
	changeListener(evt) {
		this.showInsertBtn = false;
		
		var newArray = [];
		var files = evt.target.files; // FileList object
		var file = files[0];
		
		console.log('if');
		var reader = new FileReader();
		reader.readAsText(file);
		reader.onload = (event: any) => {
			this.headersList = [];
			this.csvData = [];
			var csv = null;
			var csvData = null;
			
			csv = event.target.result; // Content of CSV file
			this.papa.parse(csv, {
				skipEmptyLines: true,
				header: false,
				complete: (results) => {
					this.showInsertBtn = true;
					for (let i = 0; i < results.data.length; i++) {
						let headerDetails = results.data[i];
						this.headers.push(headerDetails);
					}
				}
			});

			if (this.headers[0]) {
				this.compareCsvField(this.headers[0], this.availableHeader);
				this.headersList = this.headers[0];
				var index = this.headersList.indexOf('gender')
				csvData = this.headers.filter(function (item, index) {
					if (index > 0) {
						return item;
					}
				});			
				this.csvData = csvData;
			}
		}

	}

	showSelectedHeader(Value, i) {
		var index = this.columndata.findIndex(ele => ele.name.toLowerCase() === Value.toLowerCase())
		return index === -1 ? `Column ${i}` : this.columndata[index].value
	}

	submitInsertLeadApi() {
		if (this.loading) return
		this.loading = true
		var leadsApiData = []
		this.csvData.forEach(element => {
			let tempArray = {};
			var LeadsData = {};
			var additional_field = {};

			this.columndata.forEach((colvalue, index) => {
				var array2 = this.headersList.map(function (x) { return x.toLowerCase() })
				var index = array2.indexOf(colvalue.name.toLowerCase());
				var currColumn = colvalue.value;
				if (index >= 0) {
					if (currColumn === 'Additional_Field') {
						additional_field[colvalue.name] = element[index];
						
						console.log('additional_field',additional_field);
					}
					else {
						if (colvalue.value === "Pipeline" || colvalue.value === "Source" || colvalue.value === "Status") {
							LeadsData[currColumn] = element[index]
						} else {
							tempArray[currColumn] = element[index];
						}
					}

					if (additional_field && (Object.keys(additional_field).length)) {
						tempArray["additional_data"] = additional_field
					}

					tempArray["LeadsData"] = LeadsData
					
				}
			});
			leadsApiData.push(tempArray);
		});

		const body = {
			CompanyNum: this.cNum,
			csvData: leadsApiData
		}
		console.log(body);
		this.apiService.insertLead(body).subscribe((data: any) => {
			console.log(data);
			this.showToster('success', 'Data Inserted Successfully')
			this.updateData.updateSelectColumnData([])
			this.loading = false;


		}, (err) => {
			this.showToster('error', 'Please provide valid column_names/data type')
			console.log(err)
			this.loading = false;
		})
	}

	submitInsertActiveApi() {
		if (this.loading) return
		this.loading = true
		var insertApiData = []
		this.csvData.forEach(element => {
			var additional_field = {};
			let tempArray = {};
			var MemberShip = {}
			this.columndata.forEach((colvalue, index) => {
				var array2 = this.headersList.map(function (x) { return x.toLowerCase() })
				var index = array2.indexOf(colvalue.name.toLowerCase());
				var currColumn = colvalue.value;
				if (index >= 0) {
					if (currColumn === 'Additional_Field') {
						additional_field[colvalue.name] = element[index];
					} else {
						if (colvalue.value === "TrueBalanceValue" || colvalue.value === "MemberShip" || colvalue.value === "VaildDate" || colvalue.value === "StartDate") {
							MemberShip[currColumn] = element[index]
						} else {
							tempArray[currColumn] = element[index];
						}
					}

					tempArray["MembershipData"] = MemberShip
					if (additional_field && (Object.keys(additional_field).length)) {
						tempArray["additional_data"] = additional_field
					}
				}

			});
			insertApiData.push(tempArray);
		});

		const body = {
			CompanyNum: this.cNum,
			csvData: insertApiData
		}
		console.log(body);
		this.apiService.insertActive(body).subscribe((data: any) => {
			console.log(data);
			this.showToster('success', 'Data Inserted Successfully')
			this.updateData.updateSelectColumnData([])
			this.loading = false
		}, (err) => {
			this.showToster('error', 'Please provide valid column_names/data type')
			this.loading = false
			console.log(err)
		})
	}

	submitInsertArchiveApi() {
		if (this.loading) return
		this.loading = true
		var insertApiData = []
		var additional_field = {};
		this.csvData.forEach(element => {
			let tempArray = {};
			this.columndata.forEach((colvalue, index) => {
				var array2 = this.headersList.map(function (x) { return x.toLowerCase() })
				var index = array2.indexOf(colvalue.name.toLowerCase());
				var currColumn = colvalue.value;
				if (index >= 0) {
					if (currColumn === 'Additional_Field') {
						additional_field[colvalue.name] = element[index];
					} else {
						tempArray[currColumn] = element[index];
					}
					if (additional_field && (Object.keys(additional_field).length)) {
						tempArray["additional_data"] = additional_field
					}
				}
			});
			insertApiData.push(tempArray);
		});

		const body = {
			CompanyNum: this.cNum,
			csvData: insertApiData
		}
		console.log(body);
		this.apiService.insertArchive(body).subscribe((data: any) => {
			console.log(data);
			this.showToster('success', 'Data Inserted Successfully')
			this.updateData.updateSelectColumnData([])
			this.loading = false
		}, (err) => {
			this.showToster('error', 'Please provide valid column_names/data type')
			console.log(err)
			this.loading = false
		})
	}

	showToster(icon: SweetAlertIcon, title: string) {
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000,
			timerProgressBar: true,
			onOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		})

		Toast.fire({
			icon: icon,
			title: title
		})
	}

}

@Component({
	selector: 'dialog-overview-example-dialog',
	templateUrl: './dialog-overview-example-dialog.html',
	styleUrls: ['./fileupload.component.css']
})
export class DialogOverviewExampleDialog {
	headersList;
	currentHeader;
	csvData;
	public cNum: string = '';
	public uId: string = '';
	public addColumn: FormGroup
	public showInput: boolean = false
	public loading: boolean = false
	public columndata: any = []
	public openModel: any
	public selectedValue: string
	
	public genderModal: boolean = false
	public genderUniqueArray: any = []
	public changeGenderArray: any = []
	
	public yesNoModal: boolean = false
	
	public getMailUniqueArray: any = []
	public changeEmailArray: any = []
	
	public getSmsUniqueArray: any = []
	public changeSmsArray: any = []
	
	public currImportType: string;
	public gender: any = [
		{ name: 'Male', value: '0' },
		{ name: 'Female', value: '1' },
		{ name: 'Other', value: '2' }
	]
	public yesNo: any = [
		{ name: 'Yes', value: '1' },
		{ name: 'No', value: '0' }
	]
	public dataType: any = [
		{ name: 'string', value: 'string' },
		{ name: 'number', value: 'number' },
		{ name: 'list', value: 'list' },
		{ name: 'radio', value: 'radio' },
		{ name: 'checkbox', value: 'checkbox' },
	]

	constructor(
		private activatedRoute: ActivatedRoute,
		private formbuilder: FormBuilder,
		private apiService: ApiServiceService,
		public updateData: UpdatedataService,
		public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
		@Inject(MAT_DIALOG_DATA)
		public data
	) {
		this.activatedRoute.queryParams.subscribe(params => {
			this.cNum = params['cNum'];
			this.uId = params['uid'];
		});
		this.headersList = data.headersList
		this.currentHeader = data.currentHeader
		this.csvData = data.csvData
		this.openModel = data.openModalHeader
		this.currImportType = data.currImportType
		this.addColumn = this.formbuilder.group({
			col_name: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
			datatype: ['', [Validators.required]]
		})

		this.updateData.columnData.subscribe((data: any) => {
			if (data) {
				this.columndata = data
			}
		})
		var possibleYesNoHeader = ['get email','get mail','getemail', 'getmail','get sms','getsms','get message']

		if (possibleYesNoHeader.indexOf(this.currentHeader.toLowerCase()) > -1) {
			this.yesNoModal = true;
			var index = this.headersList.indexOf(this.currentHeader)
			var newData = []
			this.csvData.map(ele => {
				newData.push(ele[index])
			})

			
			if(possibleYesNoHeader.indexOf(this.currentHeader.toLowerCase()) > -1){			
				this.getMailUniqueArray = [...new Set(newData)]
				for (var i = 0; i < this.getMailUniqueArray.length; i++) {
					this.changeEmailArray.push({ name: this.getMailUniqueArray[i], value: '1' })
				}
			}
			if(possibleYesNoHeader.indexOf(this.currentHeader.toLowerCase()) > -1){
				this.getSmsUniqueArray = [...new Set(newData)]
				for (var i = 0; i < this.getSmsUniqueArray.length; i++) {
					this.changeSmsArray.push({ name: this.getSmsUniqueArray[i], value: '1' })
				}
			}
		}
		if (data.selectedColumn.value.toLowerCase() === 'gender') {
			this.genderModal = true
			var index = this.headersList.indexOf(this.currentHeader)
			var array = []
			this.csvData.map(ele => {
				array.push(ele[index])
			})
			this.genderUniqueArray = [...new Set(array)]
			for (var i = 0; i < this.genderUniqueArray.length; i++) {
				this.changeGenderArray.push({ name: this.genderUniqueArray[i], value: '2' })
			}
		}

		console.log(this.currentHeader);
	}

	submit() {
		console.log(this.columndata);
		console.log("submit ===> ", this.addColumn)
		if (this.showInput) {
			if (this.addColumn.invalid) return
			if (this.loading) return
			this.loading = true
			console.log(this.addColumn);
			let data = this.addColumn.value;
			data.type = this.currImportType;
			data.uId = this.uId;
			data.CompanyNum = this.cNum;
		
			this.apiService.addNewColumn(this.addColumn.value).subscribe((data: any) => {
				// 
				var index = this.headersList.indexOf(this.currentHeader)
				this.headersList[index] = this.addColumn.value.col_name;
				this.columndata.push({ name: this.addColumn.value.col_name, value: 'Additional_Field' })
				this.updateData.updateHeadersList(this.headersList)
				// 
				this.showToster('success', 'Column added successfuly')
				this.loading = false
				this.closeDialog()
			}, (error) => {
				this.loading = false
				this.showToster('error', 'Error in adding column')
				console.log("error add column", error)
			})
		} else {
			if (!this.selectedValue) return
			// var index = this.columndata.findIndex(ele => ele.name === this.currentHeader)
			var index = this.columndata.findIndex(ele => ele.name.toLowerCase() === this.currentHeader.toLowerCase())

			if (index === -1) {
				this.columndata.push({ name: this.currentHeader, value: this.selectedValue })
			} else {
				this.columndata[index]= { name: this.currentHeader, value: this.selectedValue }

				// this.columndata.splice(index, 1)
				// this.columndata.push()
			}
			this.updateData.updateSelectColumnData(this.columndata)
			this.updateData.updateSelectGenderData({ changeGenderArray: this.changeGenderArray, header: this.currentHeader })
			this.updateData.updateSelectGenderData({ changeEmailArray: this.changeEmailArray, header: this.currentHeader })
			this.updateData.updateSelectGenderData({ changeSmsArray: this.changeSmsArray, header: this.currentHeader })
			this.selectedValue = ""
			this.closeDialog()
		}
	}

	closeDialog(): void {
		this.dialogRef.close();
	}

	selectColumn(value) {
		if (value === "new") {
			this.showInput = true
		} else {
			this.showInput = false
			this.selectedValue = value
		}
	}

	selectGender(value, gender) {
		var index = this.changeGenderArray.findIndex(ele => ele.name === gender)
		if (index === -1) {
			this.changeGenderArray.push({ name: gender, value: value })
		} else {
			this.changeGenderArray.splice(index, 1)
			this.changeGenderArray.push({ name: gender, value: value })
		}
		this.selectedValue = 'Gender'
	}

	
	selectYesNo(value, data, gettype) {
		if(gettype == 'getemail'){
			var index = this.changeEmailArray.findIndex(ele => ele.name === data)
			if (index === -1) {
				this.changeEmailArray.push({ name: data, value: value })
			} else {
				this.changeEmailArray.splice(index, 1)
				this.changeEmailArray.push({ name: data, value: value })
			}
			this.selectedValue = 'GetEmail'
			return;		
		}
		if(gettype == 'getsms'){
			var index = this.changeEmailArray.findIndex(ele => ele.name === data)
			if (index === -1) {
				this.changeEmailArray.push({ name: data, value: value })
			} else {
				this.changeEmailArray.splice(index, 1)
				this.changeEmailArray.push({ name: data, value: value })
			}
			this.selectedValue = 'GetEmail'
			return;					
		}
		
		
	}

	showToster(icon: SweetAlertIcon, title: string) {
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000,
			onOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		})

		Toast.fire({
			icon: icon,
			title: title
		})
	}

}
