import { Routes } from '@angular/router';

import { FileuploadComponent } from './fileupload.component';

export const FileUploadRoutes: Routes = [{
	path: '',
	component: FileuploadComponent
}];
