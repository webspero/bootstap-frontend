import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {
public  BASE_URL = environment.BASE_URL

  constructor(private http : HttpClient) { }

  addNewColumn(body){
    return this.http.post(this.BASE_URL+'office/rest/addClientColumn.php',body)
  }
  getColumns(){
    return this.http.get(this.BASE_URL+'office/rest/getClientColumns.php')
  }

  insertActive(body){
    return this.http.post(this.BASE_URL+'office/rest/insertActiveClients.php',body)
  }

  insertArchive(body){
    return this.http.post(this.BASE_URL+'office/rest/insertArchiveClient.php',body)
  }

  insertLead(body){
    return this.http.post(this.BASE_URL+'office/rest/insertLeadsClients.php',body)
  }
}
