import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UpdatedataService {

  public columnData: BehaviorSubject<any> = new BehaviorSubject<any>([
    { name: "first name", value: "FirstName" },
    { name: "last name", value: "LastName" },
    { name: "date of birth", value: "Dob" },
    // Gender
    { name: "gender", value: "Gender" },
    { name: "sex", value: "Gender" },
    { name: "מין", value: "Gender" },
    { name: "מגדר", value: "Gender" },
    // Gender
    
    // ContactPhone
    { name: "phone number", value: "ContactPhone" },
    { name: "mobile", value: "ContactPhone" },
    { name: "phone", value: "ContactPhone" },
    { name: "mobile phone", value: "ContactPhone" },
    { name: "personal number ", value: "ContactPhone" },
    { name: "נייד", value: "ContactPhone" },
    { name: "מספר נייד", value: "ContactPhone" },
    { name: "טלפון נייד", value: "ContactPhone" },
    { name: "טלפון אישי", value: "ContactPhone" },
    { name: "מספר טלפון נייד", value: "ContactPhone" },
    { name: "מס׳ נייד", value: "ContactPhone" },
    // End ContactPhone

    // Email
    { name: "Email", value: "Email" },
    { name: "email", value: "Email" },
    { name: "mail", value: "Email" },
    { name: "מייל", value: "Email" },
    { name: "דוא״ל", value: "Email" },
    { name: "אי מייל", value: "Email" },
    { name: "דואר אלקטרוני", value: "Email" },
    { name: "כתובת מייל", value: "Email" },
    // End Email

    { name: "Get Email", value: "GetEmail" },
    { name: "Source", value: "Source" },
    { name: "Get SMS", value: "GetSMS" }
  ]);
  public genderData:  BehaviorSubject<any> = new BehaviorSubject<any>(null);
  public getEmailData:  BehaviorSubject<any> = new BehaviorSubject<any>(null);
  public getSmsData:  BehaviorSubject<any> = new BehaviorSubject<any>(null);
  public headersList:  BehaviorSubject<any> = new BehaviorSubject<any>(null);
  constructor() { }

  updateSelectColumnData(data): void {
    this.columnData.next(data);
  }

  updateSelectGenderData(data): void {
    this.genderData.next(data);
  }
  updateHeadersList(data): void {
    this.headersList.next(data);
  }
  updateGetEmailList(data): void {
    this.getEmailData.next(data);
  }
  updateGetSmsList(data): void {
    this.getSmsData.next(data);
  }
}
